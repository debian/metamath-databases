#!/bin/bash

TEMPFILE=`mktemp`
metamath 'read '"$1" 'verify proof *' 'exit' | tee "$TEMPFILE"
grep -q 'All proofs in the database were verified' "$TEMPFILE"
RES=$?
rm "$TEMPFILE"
exit $RES
